pipeline {
    agent any
    environment {
        RAMA = 'uat'
        GITLAB_USER_NAME_PIPELINE = 'Santiago Posada Hurtado'
        GITLAB_USER_EMAIL_PIPELINE = 'santiago.posada@pragma.com.co'
        GITLAB_ACCESS_TOKEN_PIPELINE = 'glpat-x7LVoMuM6d8kJdtPNV_v'
        CI_PROJECT_PATH = 'santiago.posada/docker-templates.git'
    }
    stages {
        stage('checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/uat']], extensions: [],
                userRemoteConfigs: [[credentialsId: 'GitLab-Auth-SSH-Key',
                url: 'git@gitlab.com:santiago.posada/docker-templates.git']]])
            }
        }
        stage('generateTag') {
            steps {
                sh 'git config user.email "${GITLAB_USER_EMAIL_PIPELINE}"'
                sh 'git config user.name "${GITLAB_USER_NAME_PIPELINE}"'
                //sh 'git remote set-url tag-originn https://gitlab.com/${CI_PROJECT_PATH}'
                sh 'git remote remove tag-origin'
                sh 'git remote add tag-origin https://oauth2:${GITLAB_ACCESS_TOKEN_PIPELINE}@gitlab.com/${CI_PROJECT_PATH}'
                //sh 'git config --global --unset http.proxy'
                sh 'git tag -a "tag-${RAMA}-$(date +%d%m%Y%M%S)" -m "Creación aútomatica del tag tag-${RAMA}-$(date +%d%m%Y%M%S) de la rama ${RAMA} desde jenkins"'
                sh 'git push tag-origin "tag-${RAMA}-$(date +%d%m%Y%M%S)"'
            }
        }
    }
}
