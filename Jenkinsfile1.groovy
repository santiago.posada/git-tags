pipeline {
    agent any
    environment {
        RAMA = 'master'
        GITLAB_USER_NAME_PIPELINE = 'Santiago Posada Hurtado'
        GITLAB_USER_EMAIL_PIPELINE = 'santiago.posada@pragma.com.co'
        GITLAB_ACCESS_TOKEN_PIPELINE = 'glpat-x7LVoMuM6d8kJdtPNV_v'
        CI_PROJECT_PATH = 'santiago.posada/docker-templates.git'
    }
    stages {
        stage('checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/master']], extensions: [],
                userRemoteConfigs: [[credentialsId: 'GitLab-Auth-SSH-Key-Jenkins',
                url: 'git@gitlab.com:santiago.posada/docker-templates.git']]])
            }
        }
        stage('generateTag') {
            steps {
                sh 'git config user.email "${GITLAB_USER_EMAIL_PIPELINE}"'
                sh 'git config user.name "${GITLAB_USER_NAME_PIPELINE}"'
                sh 'git remote -v'
                sh 'git remote remove origin'
                sh 'git remote add origin https://oauth2:${GITLAB_ACCESS_TOKEN_PIPELINE}@gitlab.com/${CI_PROJECT_PATH}'
                sh 'git tag -a "tag-${RAMA}-$(date +%d%m%Y%M)" -m "Creación aútomatica del tag tag-${RAMA}-$(date +%d%m%Y%M) de la rama ${RAMA} desde jenkins"'
                sh 'git push origin "tag-${RAMA}-$(date +%d%m%Y%M)"'
            }
        }
    }
}
