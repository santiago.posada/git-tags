def RAMA = 'uat'
def GITLAB_USER_NAME_PIPELINE = 'Santiago Posada Hurtado'
def GITLAB_USER_EMAIL_PIPELINE = 'santiago.posada@pragma.com.co'
def GITLAB_ACCESS_TOKEN_PIPELINE = 'glpat-x7LVoMuM6d8kJdtPNV_v'
def CI_PROJECT_PATH = 'santiago.posada/docker-templates.git'

node{
    stage ('clone_repo') {
        git branch: RAMA, credentialsId: 'GitLab-Auth-SSH-Key-Jenkins', url: 'git@gitlab.com:santiago.posada/docker-templates.git'
    }
    stage ('generate_tag'){
        sh 'git config user.email "'+GITLAB_USER_EMAIL_PIPELINE+'"'
        sh 'git config user.name "'+GITLAB_USER_NAME_PIPELINE+'"'
        sh 'git remote -v'
        sh 'git tag -a "tag-'+RAMA+'-$(date +%d%m%Y%M)" -m "Creación aútomatica del tag tag-'+RAMA+'-$(date +%d%m%Y%M) de la rama '+RAMA+' desde jenkins"'
        sh 'git push origin "tag-'+RAMA+'-$(date +%d%m%Y%M)"'
    }
}